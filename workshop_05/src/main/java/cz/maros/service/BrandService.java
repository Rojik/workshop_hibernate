package cz.maros.service;

import cz.maros.model.Brand;

public interface BrandService {

    public Brand getBrandById(Long id);
}
