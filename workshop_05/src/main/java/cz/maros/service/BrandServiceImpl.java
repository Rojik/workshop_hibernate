package cz.maros.service;

import cz.maros.dao.BrandDAO;
import cz.maros.model.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandDAO brandDAO;

    @Transactional
    @Override
    public Brand getBrandById(Long id) {
        return brandDAO.findBrandById(id);
    }
}
