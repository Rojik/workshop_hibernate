package cz.maros.service;

import cz.maros.model.Brand;
import cz.maros.model.Visitor;

import java.util.List;

public interface VisitorService {

    public List<Visitor> getVisitorsTriedBranch(Brand brand);
}
