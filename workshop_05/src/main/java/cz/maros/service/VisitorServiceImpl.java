package cz.maros.service;

import cz.maros.dao.VisitorDAO;
import cz.maros.model.Brand;
import cz.maros.model.Visitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VisitorServiceImpl implements VisitorService {

    @Autowired
    private VisitorDAO visitorDAO;

    @Transactional
    @Override
    public List<Visitor> getVisitorsTriedBranch(Brand brand) {
        return visitorDAO.findVisitorsTriedBranch(brand);
    }
}
