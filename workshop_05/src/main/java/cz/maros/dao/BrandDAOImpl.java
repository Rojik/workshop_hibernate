package cz.maros.dao;

import cz.maros.model.Brand;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class BrandDAOImpl implements BrandDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public Brand findBrandById(Long id) {
        return entityManager.find(Brand.class, id);
    }
}
