package cz.maros.dao;

import cz.maros.model.Brand;
import cz.maros.model.Visitor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class VisitorDAOImpl implements VisitorDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Visitor> findVisitorsTriedBranch(Brand brand) {
        return entityManager.createQuery("SELECT DISTINCT v FROM Visitor v INNER JOIN v.carVisitors cv INNER JOIN cv.car c INNER JOIN c.carModel cm INNER JOIN cm.brand b WHERE b.id = :id")
                .setParameter("id", brand.getId()).getResultList();
    }
}
