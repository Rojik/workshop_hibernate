package cz.maros.dao;


import cz.maros.model.Brand;

public interface BrandDAO {

    public Brand findBrandById(Long id);
}
