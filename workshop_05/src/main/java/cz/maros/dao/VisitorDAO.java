package cz.maros.dao;

import cz.maros.model.Brand;
import cz.maros.model.Visitor;

import java.util.List;

public interface VisitorDAO {

    public List<Visitor> findVisitorsTriedBranch(Brand brand);
}
