package cz.maros.controller;

import cz.maros.model.*;
import cz.maros.service.BrandService;
import cz.maros.service.VisitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WorkshopController {

    private static Logger log = LoggerFactory.getLogger(WorkshopController.class);

    @Autowired
    private BrandService brandService;

    @Autowired
    private VisitorService visitorService;


    @RequestMapping(value = "/")
    public ModelAndView actionDefault() {
        ModelAndView model = new ModelAndView("workshop");

        getQueries(model);
        model.addObject("completeQueries", "Queries was completed");

        return model;
    }

    private void getQueries(ModelAndView model) {
        model.addObject("visitorsTriedBranch1", visitorService.getVisitorsTriedBranch(brandService.getBrandById(2L)));
        model.addObject("visitorsTriedBranch2", visitorService.getVisitorsTriedBranch(brandService.getBrandById(1L)));
    }

}
