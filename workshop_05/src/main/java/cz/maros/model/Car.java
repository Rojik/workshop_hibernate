package cz.maros.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "W5_CAR")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CAR_ID_SEQ")
    @SequenceGenerator(name = "CAR_ID_SEQ", sequenceName = "CAR_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "PLATE_NUMBER")
    private String plateNumber;

    @ManyToOne
    @JoinColumn(name = "CAR_MODEL_ID")
    private CarModel carModel;

    @OneToMany(mappedBy = "car")
    private Set<CarVisit> carVisitors = new HashSet<CarVisit>();

    public Set<CarVisit> getCarVisitors() {
        return carVisitors;
    }

    public void setCarVisitors(Set<CarVisit> carVisitors) {
        this.carVisitors = carVisitors;
    }

    public CarModel getCarModel() {
        return carModel;
    }

    public void setCarModel(CarModel carModel) {
        this.carModel = carModel;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Long getId() {
        return id;
    }
}
