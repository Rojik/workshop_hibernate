package cz.maros.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "W5_CAR_VISIT")
public class CarVisit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "VISITOR_ID_SEQ")
    @SequenceGenerator(name = "VISITOR_ID_SEQ", sequenceName = "VISITOR_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_VISIT")
    @Temporal(TemporalType.DATE)
    private Date dateVisit;

    @ManyToOne
    @JoinColumn(name = "CAR_ID")
    private Car car;

    @ManyToOne
    @JoinColumn(name = "VISITOR_ID")
    private Visitor visitor;

    public Visitor getVisitor() {
        return visitor;
    }

    public void setVisitor(Visitor visitor) {
        this.visitor = visitor;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getDateVisit() {
        return dateVisit;
    }

    public void setDateVisit(Date dateVisit) {
        this.dateVisit = dateVisit;
    }
}
