package cz.maros.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "W5_VISITOR")
public class Visitor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "VISITOR_ID_SEQ")
    @SequenceGenerator(name = "VISITOR_ID_SEQ", sequenceName = "VISITOR_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "visitor")
    private Set<CarVisit> carVisitors = new HashSet<CarVisit>();

    @Override
    public String toString() {
        return "Visitor - ID:" + getId() + ", name: " + getName();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<CarVisit> getCarVisitors() {
        return carVisitors;
    }

    public void setCarVisitors(Set<CarVisit> carVisitors) {
        this.carVisitors = carVisitors;
    }
}
