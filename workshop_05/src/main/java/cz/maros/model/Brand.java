package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W5_BRAND")
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BRAND_ID_SEQ")
    @SequenceGenerator(name = "BRAND_ID_SEQ", sequenceName = "BRAND_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "brand")
    private List<CarModel> carModels = new ArrayList<CarModel>();

    public List<CarModel> getCarModels() {
        return carModels;
    }

    public void setCarModels(List<CarModel> carModels) {
        this.carModels = carModels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
