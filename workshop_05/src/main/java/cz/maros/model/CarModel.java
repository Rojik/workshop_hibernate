package cz.maros.model;


import javax.persistence.*;

@Entity
@Table(name = "W5_CAR_MODEL")
public class CarModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CAR_MODEL_ID_SEQ")
    @SequenceGenerator(name = "CAR_MODEL_ID_SEQ", sequenceName = "CAR_MODEL_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "BRAND_ID")
    private Brand brand;

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
