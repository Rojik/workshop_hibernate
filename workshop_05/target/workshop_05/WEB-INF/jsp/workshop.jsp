<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop</title>
</head>
<body>
<h2>Queries</h2>
<div><strong>1. getVisitorsTriedBranch</strong> <br />
    <p:forEach var="visitor" items="${visitorsTriedBranch1}">
        <span>${visitor}</span> <br />
    </p:forEach> <br />
    <p:forEach var="visitor" items="${visitorsTriedBranch2}">
        <span>${visitor}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h2>${completeQueries}</h2>
</body>
</html>