<%@ page import="cz.maros.model.Category" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop</title>
</head>
<body>
<h2>${importQueries}</h2>
<p>---------------------------------------------------------------------------------</p>
<h2>Queries</h2>
<h3>Simple queries</h3>
<span><strong>1. getProductByID</strong> - ${productById1}</span> <br />
<span><strong>1. getProductByID</strong> - ${productById2}</span>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getProductsBySupplierCountry</strong> <br />
    <p:forEach var="product" items="${productsBySupplierCountry1}">
        <span>${product}</span> <br />
    </p:forEach> <br />
    <strong>2. getProductsBySupplierCountry</strong> <br />
    <p:forEach var="product" items="${productsBySupplierCountry2}">
        <span>${product}</span> <br />
    </p:forEach>
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>3. getProductsInCategory</strong> <br />
    <p:forEach var="product" items="${productsInCategory1}">
        <span>${product}</span> <br />
    </p:forEach> <br />
    <strong>3. getProductsInCategory</strong> <br />
    <p:forEach var="product" items="${productsInCategory2}">
        <span>${product}</span> <br />
    </p:forEach>
</div>
<p>---------------------------------------------------------------------------------</p>
<h3>Navigation</h3>
<div><strong>4. getProductsWithCategory</strong> <br />
    <p:forEach var="category" items="${productsWithCategory}">
        <span>${category}</span><br />
    </p:forEach>
</div>
<p>---------------------------------------------------------------------------------</p>
<h3>Sub-Categories</h3>
<div><strong>5. getProductsInCategories - Notebooks</strong> <br />
    <span>${productsInCategories1}</span><br /><br />
    <strong>5. getProductsInCategories - Computers and accessories</strong> <br />
    <span>${productsInCategories2}</span><br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h3>Suppliers of</h3>
<div><strong>6. getSuppliersWithExpensiveProducts > more than 1200</strong> <br />
<p:forEach var="supplier" items="${supplierWithExpensiveProduct1}">
    <span>${supplier}</span> <br />
</p:forEach> <br />
<strong>6. getSuppliersWithExpensiveProducts > more than 10000</strong> <br />
<p:forEach var="supplier" items="${supplierWithExpensiveProduct2}">
    <span>${supplier}</span> <br />
</p:forEach>
</div>
<p>---------------------------------------------------------------------------------</p>
<strong>7. nechapu</strong>
<p>---------------------------------------------------------------------------------</p>
<h3>Shopping Baskets</h3>
<div><strong>8. getAllBasketWithProduct</strong> <br />
    <p:forEach var="basket" items="${allBasketWithProduct1}">
        <span>${basket}</span> <br />
    </p:forEach> <br />
    <strong>8. getAllBasketWithProduct</strong> <br />
    <p:forEach var="basket" items="${allBasketWithProduct2}">
        <span>${basket}</span> <br />
    </p:forEach>
</div>
<p>---------------------------------------------------------------------------------</p>
<h3>Who are these young dirty kids...</h3>
<div><strong>9. getAllUsersWithProduct</strong> <br />
    <p:forEach var="user" items="${allUsersWithProduct1}">
        <span>${user}</span> <br />
    </p:forEach> <br />
    <strong>9. getAllUsersWithProduct</strong> <br />
    <p:forEach var="user" items="${allUsersWithProduct2}">
        <span>${user}</span> <br />
    </p:forEach>
</div> <br />
<div><strong>10. getAllUsersWithProductOrder</strong> <br />
    <p:forEach var="user" items="${allUsersWithProductOrder}">
        <span>${user}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h3>... reading PlayBoy?</h3>
<div><strong>10. getAllUsersWithProductOrderCategory</strong> <br />
    <p:forEach var="user" items="${allUsersWithProductOrderCategory}">
        <span>${user}</span> <br />
    </p:forEach> <br />
</div>
<h2>${completeQueries}</h2>
</body>
</html>