package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="W_PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PRODUCT_ID_SEQ")
    @SequenceGenerator(name = "PRODUCT_ID_SEQ", sequenceName = "PRODUCT_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="NAME")
    private String name;

    @Basic
    @Column(name="PRICE")
    private int price;

    @ManyToOne
    @JoinColumn(name="CATEGORY_ID")
    private Category category;

    @ManyToOne
    @JoinColumn(name="SUPPLIER_ID")
    private Supplier supplier;

    @ManyToMany(mappedBy = "products")
    private List<ShoppingBasket> shoppingBaskets = new ArrayList<ShoppingBasket>();

    public List<ShoppingBasket> getShoppingBaskets() {
        return shoppingBaskets;
    }

    public void setShoppingBaskets(List<ShoppingBasket> shoppingBaskets) {
        this.shoppingBaskets = shoppingBaskets;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Product: ID: " + getId() + ", name: " + getName() + ", price: " + getPrice();
    }
}
