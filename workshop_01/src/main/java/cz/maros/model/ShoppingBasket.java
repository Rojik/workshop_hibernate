package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="W_SHOPPING_BASKET")
public class ShoppingBasket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SHOPPING_BASKET_ID_SEQ")
    @SequenceGenerator(name = "SHOPPING_BASKET_ID_SEQ", sequenceName = "SHOPPING_BASKET_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_SHOP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @ManyToMany
    @JoinTable(
            name = "W_SHOP_B_PROD",
            joinColumns = {@JoinColumn(name = "SHOPPING_BASKET_ID")},
            inverseJoinColumns = {@JoinColumn(name = "PRODUCT_ID")})
    private List<Product> products = new ArrayList<Product>();

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Shopping basket - ID: " + getId() + ", date: " + date;
    }
}
