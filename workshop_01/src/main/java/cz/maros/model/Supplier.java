package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="W_SUPPLIER")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SUPPLIER_ID_SEQ")
    @SequenceGenerator(name = "SUPPLIER_ID_SEQ", sequenceName = "SUPPLIER_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="NAME", unique = true)
    private String name;

    @Basic
    @Column(name="STREET")
    private String street;

    @Basic
    @Column(name="HOUSE_NUMBER")
    private String houseNumber;

    @Basic
    @Column(name="POSTCODE")
    private String postcode;

    @Basic
    @Column(name="COUNTRY")
    private String country;

    @OneToMany(mappedBy = "supplier")
    private List<Product> products = new ArrayList<Product>();

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Supplier - ID: " + getId() + ", name: " + getName();
    }
}
