package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="W_CATEGORY")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CATEGORY_ID_SEQ")
    @SequenceGenerator(name = "CATEGORY_ID_SEQ", sequenceName = "CATEGORY_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    private Category parent;

    @OneToMany(mappedBy = "parent"/*, fetch = FetchType.EAGER*/)
    private List<Category> children = new ArrayList<Category>();

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private List<Product> products = new ArrayList<Product>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public String toString() {
        if(getParent() == null)
            return "Category - ID: " + getId() + ", name: " + getName() + ", parent: null";
        else
            return "Category - ID: " + getId() + ", name: " + getName() + ", parent: " + getParent().getName();
    }
}
