package cz.maros.dao;

import cz.maros.model.Category;
import cz.maros.model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("productDAO")
public class ProductDAOImpl implements ProductDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveProduct(Product product) {
        entityManager.persist(product);
    }

    @Override
    public Product findProductById(long id) {
        return entityManager.find(Product.class, id);
    }

    @Override
    public Product findProductByName(String name) {
        Product product = (Product) entityManager.createQuery("FROM Product p WHERE p.name = :name")
                .setParameter("name", name).getSingleResult();
        return product;
    }

     //"select p from Product p where p.supplier.postCode=:code"

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> findProductsWithSupplierCountry(String country) {
        return entityManager.createQuery("SELECT p FROM Product p JOIN p.supplier s WHERE s.country = :country")
                .setParameter("country", country).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> findProductsInCategory(Category category) {
        return entityManager.createQuery("SELECT p FROM Product p JOIN p.category c WHERE c.id = :category_id")
                .setParameter("category_id", category.getId()).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> findProductsWithCategory() {
        return entityManager.createQuery("SELECT p FROM Product p JOIN p.category c").getResultList();
    }
}
