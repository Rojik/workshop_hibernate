package cz.maros.dao;

import cz.maros.model.Category;

import java.util.List;

public interface CategoryDAO {

    public void saveCategory(Category category);

    public Category findCategoryById(long id);

    public Category findCategoryByName(String name);

    public Category findProductsInCategories(Category category);
}
