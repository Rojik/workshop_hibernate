package cz.maros.dao;

import cz.maros.model.Category;
import cz.maros.model.Product;

import java.util.List;

public interface ProductDAO {

    public void saveProduct(Product product);

    public Product findProductById(long id);

    public Product findProductByName(String name);

    public List<Product> findProductsWithSupplierCountry(String country);

    public List<Product> findProductsInCategory(Category category);

    public List<Product> findProductsWithCategory();

}
