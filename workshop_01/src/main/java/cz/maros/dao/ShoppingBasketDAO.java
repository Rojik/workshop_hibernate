package cz.maros.dao;

import cz.maros.model.Product;
import cz.maros.model.ShoppingBasket;

import java.util.List;

public interface ShoppingBasketDAO {

    public void saveShoppingBasket(ShoppingBasket shoppingBasket);

    public List<ShoppingBasket> findAllBasketWithProduct(Product product);
}
