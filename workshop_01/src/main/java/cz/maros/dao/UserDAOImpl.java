package cz.maros.dao;

import cz.maros.model.Category;
import cz.maros.model.Product;
import cz.maros.model.ShoppingBasket;
import cz.maros.model.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.List;

@Repository("userDAO")
public class UserDAOImpl implements UserDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveUser(User user) {
        entityManager.persist(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllUsersWithProduct(Product product) {
       return entityManager.createQuery(findAllUsersWithProductString())
                .setParameter("product_id", product.getId()).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllUsersWithProductOrder(Product product, Category category) {
        return entityManager.createQuery(findAllUsersWithProductString() + " ORDER BY u.birthDate ASC")
                .setParameter("product_id", product.getId()).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<User> findAllUsersWithProductOrder(Product product) {
        return entityManager.createQuery(findAllUsersWithProductString() + " ORDER BY u.name DESC")
                .setParameter("product_id", product.getId()).getResultList();
    }


    private String findAllUsersWithProductString() {
        return "SELECT u FROM User u JOIN u.shoppingBaskets b JOIN b.products p WHERE p.id = :product_id";
    }
}
