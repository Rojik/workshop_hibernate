package cz.maros.dao;

import cz.maros.model.Product;
import cz.maros.model.ShoppingBasket;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("shoppingBasketDAO")
public class ShoppingBasketDAOImpl implements ShoppingBasketDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveShoppingBasket(ShoppingBasket shoppingBasket) {
        entityManager.persist(shoppingBasket);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ShoppingBasket> findAllBasketWithProduct(Product product) {
        return entityManager.createQuery("SELECT b FROM ShoppingBasket b JOIN b.products p WHERE p.id = :product")
                .setParameter("product", product.getId()).getResultList();
    }
}
