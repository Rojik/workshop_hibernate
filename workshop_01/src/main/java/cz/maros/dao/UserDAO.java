package cz.maros.dao;

import cz.maros.model.Category;
import cz.maros.model.Product;
import cz.maros.model.User;

import java.util.List;

public interface UserDAO {

    public void saveUser(User user);

    public List<User> findAllUsersWithProduct(Product product);

    public List<User> findAllUsersWithProductOrder(Product product, Category category);

    public List<User> findAllUsersWithProductOrder(Product product);
}
