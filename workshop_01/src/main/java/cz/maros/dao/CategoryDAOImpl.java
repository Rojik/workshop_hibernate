package cz.maros.dao;

import cz.maros.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("categoryDAO")
public class CategoryDAOImpl implements CategoryDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveCategory(Category category) {
//        if(category.getId() != null) {
//            entityManager.persist(entityManager.merge(category));
//        }
//        else {
            entityManager.persist(category);
//        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Category findCategoryById(long id) {
        return entityManager.find(Category.class, id);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Category findCategoryByName(String name) {
        Category category = (Category) entityManager.createQuery("FROM Category c WHERE c.name = :name")
                .setParameter("name", name).getSingleResult();
        return category;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Category findProductsInCategories(Category category) {
        return (Category) entityManager.createQuery("SELECT c FROM Category c WHERE c.id = :category_id")
                .setParameter("category_id", category.getId()).getSingleResult();
    }


}
