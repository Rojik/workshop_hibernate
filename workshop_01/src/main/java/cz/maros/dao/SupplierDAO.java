package cz.maros.dao;

import cz.maros.model.Supplier;

import java.util.List;

public interface SupplierDAO {

    public void saveSupplier(Supplier supplier);

    public Supplier findSupplierByName(String name);

    public List<Supplier> findSuppliersWithExpensiveProducts(int price);
}
