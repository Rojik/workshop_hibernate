package cz.maros.dao;

import cz.maros.model.Supplier;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("supplierDAO")
public class SupplierDAOImpl implements SupplierDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void saveSupplier(Supplier supplier) {
        entityManager.persist(supplier);
    }

    @Override
    public Supplier findSupplierByName(String name) {
        Supplier supplier = (Supplier) entityManager.createQuery("FROM Supplier s WHERE s.name = :name")
                .setParameter("name", name).getSingleResult();
        return supplier;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Supplier> findSuppliersWithExpensiveProducts(int price) {
        return entityManager.createQuery("SELECT DISTINCT s FROM Supplier s JOIN s.products p WHERE p.price > :price")
                .setParameter("price", price).getResultList();
    }
}
