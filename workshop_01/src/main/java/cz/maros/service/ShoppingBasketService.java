package cz.maros.service;

import cz.maros.model.Product;
import cz.maros.model.ShoppingBasket;

import java.util.List;

public interface ShoppingBasketService {

    public void insertShoppingBasket(ShoppingBasket shoppingBasket);

    public List<ShoppingBasket> getAllBasketWithProduct(Product product);

}
