package cz.maros.service;

import cz.maros.dao.ProductDAO;
import cz.maros.exception.ProductNotFound;
import cz.maros.model.Category;
import cz.maros.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    @Override
    public void insertProduct(Product product) {
        productDAO.saveProduct(product);
    }

    @Transactional
    @Override
    public Product getProductByName(String name) {
        Product product = productDAO.findProductByName(name);
        if(product == null)
            throw new ProductNotFound("Product with name " + name);
        return product;
    }

    @Transactional
    @Override
    public Product getProductById(long id) {
        Product product = productDAO.findProductById(id);
        if(product == null)
            throw new ProductNotFound("Product with ID " + id);
        return product;
    }

    @Transactional
    @Override
    public List<Product> getProductsWithSupplierCountry(String country) {
        return productDAO.findProductsWithSupplierCountry(country);
    }

    @Transactional
    @Override
    public List<Product> getProductsInCategory(Category category) {
        return productDAO.findProductsInCategory(category);
    }

    @Transactional
    @Override
    public List<String> getProductsWithCategory() {
        List<Product> temp = productDAO.findProductsWithCategory();
        List<String> products = new ArrayList<String>();
        for (Product product : temp) {
            StringBuilder string = new StringBuilder();
            Category productCategory = product.getCategory();
            string.append("Product <strong>" + product.getName() + "</strong>: - " + productCategory.getName() + " - ");
            while(productCategory.getParent() != null) {
                string.append(productCategory.getParent().getName() + " - ");
                productCategory = productCategory.getParent();
            }
            products.add(string.toString());
        }
        return products;
    }

}
