package cz.maros.service;

import cz.maros.dao.UserDAO;
import cz.maros.model.Category;
import cz.maros.model.Product;
import cz.maros.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Transactional
    @Override
    public void insertUser(User user) {
        userDAO.saveUser(user);
    }

    @Transactional
    @Override
    public List<User> getAllUsersWithProduct(Product product) {
        return userDAO.findAllUsersWithProduct(product);
    }

    @Transactional
    @Override
    public List<User> getAllUsersWithProductOrder(Product product, Category category) {
        return userDAO.findAllUsersWithProductOrder(product, category);
    }

    @Transactional
    @Override
    public List<User> getAllUsersWithProductOrder(Product product) {
        return userDAO.findAllUsersWithProductOrder(product);
    }
}
