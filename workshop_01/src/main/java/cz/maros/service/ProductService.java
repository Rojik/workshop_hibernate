package cz.maros.service;

import cz.maros.model.Category;
import cz.maros.model.Product;

import java.util.List;

public interface ProductService {

    public void insertProduct(Product product);

    public Product getProductByName(String name);

    public Product getProductById(long id);

    public List<Product> getProductsWithSupplierCountry(String country);

    public List<Product> getProductsInCategory(Category category);

    public List<String> getProductsWithCategory();
}
