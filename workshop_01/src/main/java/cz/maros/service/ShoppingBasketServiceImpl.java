package cz.maros.service;

import cz.maros.dao.ShoppingBasketDAO;
import cz.maros.model.Product;
import cz.maros.model.ShoppingBasket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShoppingBasketServiceImpl implements ShoppingBasketService {

    @Autowired
    private ShoppingBasketDAO shoppingBasketDAO;

    @Transactional
    @Override
    public void insertShoppingBasket(ShoppingBasket shoppingBasket) {
        shoppingBasketDAO.saveShoppingBasket(shoppingBasket);
    }

    @Transactional
    @Override
    public List<ShoppingBasket> getAllBasketWithProduct(Product product) {
        return shoppingBasketDAO.findAllBasketWithProduct(product);
    }
}
