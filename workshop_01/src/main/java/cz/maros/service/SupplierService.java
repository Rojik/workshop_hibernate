package cz.maros.service;


import cz.maros.model.Supplier;

import java.util.List;

public interface SupplierService {

    public void insertSupplier(Supplier supplier);

    public Supplier getSupplierByName(String name);

    public List<Supplier> getSuppliersWithExpensiveProducts(int price);

}
