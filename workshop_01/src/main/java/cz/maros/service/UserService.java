package cz.maros.service;

import cz.maros.model.Category;
import cz.maros.model.Product;
import cz.maros.model.User;

import java.util.List;

public interface UserService {

    public void insertUser(User user);

    public List<User> getAllUsersWithProduct(Product product);

    public List<User> getAllUsersWithProductOrder(Product product, Category category);

    public List<User> getAllUsersWithProductOrder(Product product);
}
