package cz.maros.service;

import cz.maros.model.Category;

import java.util.List;

public interface CategoryService {

    public void insertCategory(Category category);

    public Category getCategoryById(long id);

    public Category getCategoryByName(String name);

    public String getProductsInCategories(Category category);

}
