package cz.maros.service;

import cz.maros.dao.SupplierDAO;
import cz.maros.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDAO supplierDAO;

    @Transactional
    @Override
    public void insertSupplier(Supplier supplier) {
        supplierDAO.saveSupplier(supplier);
    }

    @Transactional
    @Override
    public Supplier getSupplierByName(String name) {
        return supplierDAO.findSupplierByName(name);
    }

    @Transactional
    @Override
    public List<Supplier> getSuppliersWithExpensiveProducts(int price) {
        return supplierDAO.findSuppliersWithExpensiveProducts(price);
    }
}
