package cz.maros.service;

import cz.maros.dao.CategoryDAO;
import cz.maros.model.Category;
import cz.maros.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryDAO categoryDAO;

    @Transactional
    @Override
    public void insertCategory(Category category) {
        categoryDAO.saveCategory(category);
    }

    @Transactional
    @Override
    public Category getCategoryById(long id) {
        return categoryDAO.findCategoryById(id);
    }

    @Transactional
    @Override
    public Category getCategoryByName(String name) {
        return categoryDAO.findCategoryByName(name);
    }

    @Transactional
    @Override
    public String getProductsInCategories(Category category) {
        Category mainCategory = categoryDAO.findProductsInCategories(category);
        List<Category> knownCategories = new ArrayList<Category>();
        knownCategories.add(mainCategory);
        readCategories(mainCategory, knownCategories);

        return productsName(knownCategories);
    }

    private String productsName(List<Category> categories) {
        StringBuilder products = new StringBuilder();

        for (Category category : categories) {
            for (Product product : category.getProducts()) {
                products.append(product.getName() + ", ");
            }
        }

        return products.toString();
    }

    private void readCategories(Category parent, List<Category> knownCategories) {
        log.info("Parent: {}, count: {}", parent, knownCategories.size());
        List<Category> children = parent.getChildren();

        knownCategories.addAll(children);
        for (Category child : children) {
            readCategories(child, knownCategories);
        }
    }
}
