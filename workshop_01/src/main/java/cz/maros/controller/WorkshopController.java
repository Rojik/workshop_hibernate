package cz.maros.controller;

import cz.maros.model.*;
import cz.maros.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class WorkshopController {

    private static Logger log = LoggerFactory.getLogger(WorkshopController.class);

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingBasketService shoppingBasketService;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private UserService userService;

    @PersistenceContext
    private EntityManager entityManager;

    private final int SIZE = 20;

    @RequestMapping(value = "/")
    public ModelAndView actionDefault(ModelMap map) {
        ModelAndView model = new ModelAndView("workshop");
//        createObjects();
        model.addObject("importQueries", "Import was completed");
        getQueries(model);
        model.addObject("completeQueries", "Queries was completed");

        return model;
    }

    @RequestMapping(value = "/insertSupplier", method = RequestMethod.GET)
    public ModelAndView insertSupplierDetail() {
        ModelAndView mav = new ModelAndView("insertSupplier");
        Supplier supplier = new Supplier();
        mav.addObject("insertSupplier", supplier);
        mav.addObject("status", "success");
        return mav;
    }

    @RequestMapping(value = "/insertSupplier", method = RequestMethod.POST)
    public ModelAndView insertSupplier(@ModelAttribute("insertUser") Supplier supplier) {
        ModelAndView mav = new ModelAndView("insertSupplier");
        try {
            supplierService.insertSupplier(supplier);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mav.addObject("insertSupplier", supplier);
        return mav;
    }

    private void getQueries(ModelAndView model) {
        model.addObject("productById1", productService.getProductById(1L));
        model.addObject("productById2", productService.getProductById(3L));
        model.addObject("productsBySupplierCountry1", productService.getProductsWithSupplierCountry("Pisek"));
        model.addObject("productsBySupplierCountry2", productService.getProductsWithSupplierCountry("Blatna"));
        model.addObject("productsInCategory1", productService.getProductsInCategory(categoryService.getCategoryById(3)));
        model.addObject("productsInCategory2", productService.getProductsInCategory(categoryService.getCategoryById(6)));
        model.addObject("productsWithCategory", productService.getProductsWithCategory());
        model.addObject("productsInCategories1", categoryService.getProductsInCategories(categoryService.getCategoryById(3L)));
        model.addObject("productsInCategories2", categoryService.getProductsInCategories(categoryService.getCategoryById(1L)));
        model.addObject("supplierWithExpensiveProduct1", supplierService.getSuppliersWithExpensiveProducts(1200));
        model.addObject("supplierWithExpensiveProduct2", supplierService.getSuppliersWithExpensiveProducts(10000));
        model.addObject("allBasketWithProduct1", shoppingBasketService.getAllBasketWithProduct(productService.getProductById(1L)));
        model.addObject("allBasketWithProduct2", shoppingBasketService.getAllBasketWithProduct(productService.getProductById(3L)));
        model.addObject("allUsersWithProduct1", userService.getAllUsersWithProduct(productService.getProductById(1L)));
        model.addObject("allUsersWithProduct2", userService.getAllUsersWithProduct(productService.getProductById(3L)));
        model.addObject("allUsersWithProductOrder", userService.getAllUsersWithProductOrder(productService.getProductById(1L)));
        model.addObject("allUsersWithProductOrderCategory", userService.getAllUsersWithProductOrder(productService.getProductById(1L), categoryService.getCategoryById(2L)));
    }

    private void createObjects() {
        createCategories();
        createSuppliers();
        createProducts();
        createUsers();
    }

    private void createCategories() {
        List<Category> list = new ArrayList<Category>();
        Category parent = new Category();
        parent.setName("Computers and accessories");
        categoryService.insertCategory(parent);

        String[] categories = new String[]{"Computers", "Notebooks", "Desktops", "Software", "Magazines"};
        String[] subcategories = new String[]{"Procesors", "RAM"};
        for (String categoryTemp : categories) {
            Category category = new Category();
            category.setName(categoryTemp);
            category.setParent(parent);
            categoryService.insertCategory(category);
            list.add(category);
        }
        int i = 0;
        for (String categoryTemp : subcategories) {
            Category category = new Category();
            category.setName(categoryTemp);
            category.setParent(list.get(i));
            categoryService.insertCategory(category);
            i++;
        }
    }

//    private void saveSubcategories(Category parent, String[] categories) {
//        for (String categoryTemp : categories) {
//            Category category = new Category();
//            category.setName(categoryTemp);
//            category.setParent(parent);
//            categoryService.insertCategory(category);
//        }
//    }

    private void createSuppliers() {
        String[][] suppliers = new String[][]{{"Apple", "Pisek", "513", "35741", "Pisecka"}, {"HP", "Strakonice", "864", "45875", "Strakonicka"},
                {"Microsoft", "Praha", "362", "12345", "Prazska"}, {"Condé Nast", "Blatna", "828", "38801", "Hradska"}};
        Supplier supplier;
        for (String[] suppliersTemp : suppliers) {
            supplier = new Supplier();
            supplier.setName(suppliersTemp[0]);
            supplier.setCountry(suppliersTemp[1]);
            supplier.setHouseNumber(suppliersTemp[2]);
            supplier.setPostcode(suppliersTemp[3]);
            supplier.setStreet(suppliersTemp[4]);
            supplierService.insertSupplier(supplier);
        }

    }

    private void createProducts() {
        String[] categories = new String[]{"Notebooks", "Software", "Magazines", "Magazines", "Procesors", "RAM"};
        String[] suppliers = new String[]{"Apple", "Microsoft", "Condé Nast", "Condé Nast", "Apple", "Microsoft"};
        Category category;
        Supplier supplier;

        String[][] products = new String[][]{{"MacBook", "15321"}, {"Windows Vista", "125"}, {"Wired", "15631"}, {"Blesk", "23"}, {"Intel i5", "5699"}, {"RAM 8GB", "1299"}};
        Product product;

        for (int i = 0; i < 6; i++) {
            category = categoryService.getCategoryByName(categories[i]);
            supplier = supplierService.getSupplierByName(suppliers[i]);

            product = new Product();
            product.setName(products[i][0]);
            product.setPrice(Integer.parseInt(products[i][1]));
            product.setCategory(category);
            product.setSupplier(supplier);

            productService.insertProduct(product);
        }

    }

    private void createUsers() {
        String[][] products = new String[][]{{"MacBook"}, {"Wired", "Windows Vista"}, {"MacBook"}};

        String[][] users = new String[][]{{"George", "1993-12-16", "Straky", "513", "36541", "Idos"}, {"Alex", "1997-07-11", "New York", "89641", "1315641", "Brondwey"},
                {"Phil", "2002-04-16", "Hradec", "111", "22222", "Kominicka"}};
        int i = 0;
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");

        for (String[] userTemp : users) {
            User user = new User();
            user.setName(userTemp[0]);
            try {
                user.setBirthDate(ft.parse(userTemp[1]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            user.setCountry(userTemp[2]);
            user.setHouseNumber(userTemp[3]);
            user.setPostcode(userTemp[4]);
            user.setStreet(userTemp[4]);

            userService.insertUser(user);
            createShoppingBasket(user, products[i]);
            i++;
        }
    }

    private void createShoppingBasket(User user, String[] products) {
        List<Product> productsBasket = new ArrayList<Product>();
        ShoppingBasket shoppingBasket = new ShoppingBasket();
        shoppingBasket.setDate(new Date());
        shoppingBasket.setUser(user);

        for (String productBasket : products) {
            productsBasket.add(productService.getProductByName(productBasket));
        }
        shoppingBasket.setProducts(productsBasket);
        shoppingBasketService.insertShoppingBasket(shoppingBasket);
    }
}
