<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>
<html dir="ltr" lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset=utf-8 />
    <title>Add supplier</title>
</head>
<body>
<h1>Add supplier</h1>

<form:form commandName="insertSupplier" method="POST" id="supplierdetailsid">
    <fieldset>
        <legend>Supplier details</legend>
        <ol>
            <li>
                <label for=name>Name</label>
                <form:input path="name" type="text" />
            </li>
            <li>
                <label for=country>Country</label>
                <form:input path="country" type="text" />
            </li>
            <%--<li>--%>
                <%--<label for=house_number>Name</label>--%>
                <%--<form:input path="house_number" type="text" />--%>
            <%--</li>--%>
            <li>
                <label for=postcode>Postcode</label>
                <form:input path="postcode" type="text" />
            </li>
            <li>
                <label for=street>Street</label>
                <form:input path="street" type="text" />
            </li>
        </ol>
    </fieldset>
    <fieldset>
        <button type="submit">Save supplier</button>
    </fieldset>
</form:form>


</body>
</html>