package cz.maros.controller;

import cz.maros.model.*;
import cz.maros.service.CategoryService;
import cz.maros.service.ContactService;
import cz.maros.service.EBayItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WorkshopController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private EBayItemService eBayItemService;

    private static Logger log = LoggerFactory.getLogger(WorkshopController.class);

    @RequestMapping(value = "/")
    public ModelAndView actionDefault(ModelMap map) {
        ModelAndView model = new ModelAndView("workshop");

        getQueries(model);
        model.addObject("completeQueries", "Queries was completed");

        return model;
    }

    private void getQueries(ModelAndView model) {
        model.addObject("allItemsOrderPrice", eBayItemService.getAllItemsOrderPrice());
        model.addObject("allItemsOrderByHigherPrice1", eBayItemService.getAllItemsOrderByHigherPrice(30));
        model.addObject("allItemsOrderByHigherPrice2", eBayItemService.getAllItemsOrderByHigherPrice(150));
        model.addObject("allContactAcceptCC", contactService.getAllContactAcceptCC());
        model.addObject("allItemsByLowerPriceAndPP1", eBayItemService.getAllItemsByLowerPriceAndPP(150));
        model.addObject("allItemsByLowerPriceAndPP2", eBayItemService.getAllItemsByLowerPriceAndPP(50));
        model.addObject("allItemsBySpecifiedAddress", eBayItemService.getAllItemsBySpecifiedAddress("Las Vegas"));
        model.addObject("allCategoriesWithObjectInCity", categoryService.getAllCategoriesWithObjectInCity("San Francisco"));
        model.addObject("allCategoriesNonLeaf", categoryService.getAllCategoriesNonLeaf());
    }

}
