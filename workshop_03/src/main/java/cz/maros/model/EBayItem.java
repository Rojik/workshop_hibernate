package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name = "W3_EBAYITEM")
public class EBayItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "EBAYITEM_ID_SEQ")
    @SequenceGenerator(name = "EBAYITEM_ID_SEQ", sequenceName = "EBAYITEM_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "TITLE")
    private String title;

    @Basic
    @Column(name = "DESCRIPTION")
    private String description;

    @Basic
    @Column(name = "DETAILS")
    private String details;

    @Basic
    @Column(name = "PRICE_ASKED")
    private double priceAsked;

    @Basic
    @Column(name = "PRICE_PAID")
    private double pricePaid;

    @ManyToOne
    @JoinColumn(name = "BUYER_ID")
    private Contact buyer;

    @ManyToOne
    @JoinColumn(name = "SELLER_ID")
    private Contact seller;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    private Category category;

    @Override
    public String toString() {
        return "EBayItem - ID: " + getId() + ", title: " + getTitle() + ", price asked: " + getPriceAsked() + ", price paid: " + getPricePaid();
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Contact getBuyer() {
        return buyer;
    }

    public void setBuyer(Contact buyer) {
        this.buyer = buyer;
    }

    public Contact getSeller() {
        return seller;
    }

    public void setSeller(Contact seller) {
        this.seller = seller;
    }

    public double getPriceAsked() {
        return priceAsked;
    }

    public void setPriceAsked(double priceAsked) {
        this.priceAsked = priceAsked;
    }

    public double getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(double pricePaid) {
        this.pricePaid = pricePaid;
    }

}
