package cz.maros.model;

import org.hibernate.annotations.Index;

import javax.persistence.*;

@Entity
@Table(name = "W3_ADDRESS")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ADDRESS_ID_SEQ")
    @SequenceGenerator(name = "ADDRESS_ID_SEQ", sequenceName = "ADDRESS_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "STREET")
    private String street;

    @Basic
    @Column(name = "ZIP_CODE")
    private String zipCode;

    @Basic
    @Column(name = "CITY")
    private String city;

    @Basic
    @Column(name = "COUNTRY")
    private String country;

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
