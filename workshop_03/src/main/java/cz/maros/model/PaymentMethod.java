package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name = "W3_PAYMENTMETHOD")
public class PaymentMethod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PAYMENT_METHOD_ID_SEQ")
    @SequenceGenerator(name = "PAYMENT_METHOD_ID_SEQ", sequenceName = "PAYMENT_METHOD_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    private PaymentType type;

    @Basic
    @Column(name = "DETAILS")
    private String details;

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Long getId() {
        return id;
    }

    public PaymentType getType() {
        return type;
    }

    public void setType(PaymentType type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
