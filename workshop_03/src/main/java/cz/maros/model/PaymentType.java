package cz.maros.model;

public enum PaymentType {

    CC, PP, CA;
}
