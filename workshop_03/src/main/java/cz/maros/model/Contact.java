package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W3_CONTACT")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CONTACT_ID_SEQ")
    @SequenceGenerator(name = "CONTACT_ID_SEQ", sequenceName = "CONTACT_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Basic
    @Column(name = "LAST_NAME")
    private String lastName;

    @Basic
    @Column(name = "EMAIL")
    private String email;

    @OneToMany(mappedBy = "buyer")
    List<EBayItem> buyerItems = new ArrayList<EBayItem>();

    @OneToMany(mappedBy = "seller")
    List<EBayItem> sellerItems = new ArrayList<EBayItem>();

    @OneToMany(mappedBy = "contact")
    List<Address> addresses = new ArrayList<Address>();

    @OneToMany(mappedBy = "contact")
    List<PaymentMethod> paymentMethods = new ArrayList<PaymentMethod>();

    @Override
    public String toString() {
        return "Contact - ID: " + getId() + ", name: " + getFirstName() + " " + getLastName();
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<EBayItem> getBuyerItems() {
        return buyerItems;
    }

    public void setBuyerItems(List<EBayItem> buyerItems) {
        this.buyerItems = buyerItems;
    }

    public List<EBayItem> getSellerItems() {
        return sellerItems;
    }

    public void setSellerItems(List<EBayItem> sellerItems) {
        this.sellerItems = sellerItems;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
