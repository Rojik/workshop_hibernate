package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="W3_CATEGORY")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CATEGORY_ID_SEQ")
    @SequenceGenerator(name = "CATEGORY_ID_SEQ", sequenceName = "CATEGORY_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="TITLE")
    private String title;

    @Basic
    @Column(name="DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "CATEGORY_ID")
    private Category parent;

    @OneToMany(mappedBy = "parent")
    private List<Category> children = new ArrayList<Category>();

    @OneToMany(mappedBy = "category")
    private List<EBayItem> eBayItems = new ArrayList<EBayItem>();

    public List<EBayItem> geteBayItems() {
        return eBayItems;
    }

    public void seteBayItems(List<EBayItem> eBayItems) {
        this.eBayItems = eBayItems;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public List<Category> getChildren() {
        return children;
    }

    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public String toString() {
        if(getParent() == null)
            return "Category - ID: " + getId() + ", name: " + getTitle() + ", parent: null";
        else
            return "Category - ID: " + getId() + ", name: " + getTitle() + ", parent: " + getParent().getTitle();
    }
}
