package cz.maros.service;

import cz.maros.model.Category;

import java.util.List;

public interface CategoryService {

    public List<Category> getAllCategoriesWithObjectInCity(String address);

    public List<Category> getAllCategoriesNonLeaf();

}
