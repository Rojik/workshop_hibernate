package cz.maros.service;

import cz.maros.dao.CategoryDAO;
import cz.maros.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    @Transactional
    @Override
    public List<Category> getAllCategoriesWithObjectInCity(String address) {
        return categoryDAO.findAllCategoriesWithObjectInCity(address);
    }

    @Transactional
    @Override
    public List<Category> getAllCategoriesNonLeaf() {
        return categoryDAO.findAllCategoriesNonLeaf();
    }
}
