package cz.maros.service;

import cz.maros.model.Contact;

import java.util.List;

public interface ContactService {

    public List<Contact> getAllContactAcceptCC();

}
