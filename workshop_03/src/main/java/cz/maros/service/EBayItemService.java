package cz.maros.service;

import cz.maros.model.EBayItem;

import java.util.List;

public interface EBayItemService {

    public List<EBayItem> getAllItemsOrderPrice();

    public List<EBayItem> getAllItemsOrderByHigherPrice(double price);

    public List<EBayItem> getAllItemsByLowerPriceAndPP(double price);

    public List<EBayItem> getAllItemsBySpecifiedAddress(String address);
}
