package cz.maros.service;

import cz.maros.dao.EBayItemDAO;
import cz.maros.model.EBayItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EBayItemServiceImpl implements EBayItemService {

    @Autowired
    private EBayItemDAO eBayItemDAO;

    @Transactional
    @Override
    public List<EBayItem> getAllItemsOrderPrice() {
        return eBayItemDAO.findAllItemsOrderPrice();
    }

    @Transactional
    @Override
    public List<EBayItem> getAllItemsOrderByHigherPrice(double price) {
        return eBayItemDAO.findAllItemsOrderByHigherPrice(price);
    }

    @Override
    public List<EBayItem> getAllItemsByLowerPriceAndPP(double price) {
        return eBayItemDAO.findAllItemsByLowerPriceAndPP(price);
    }

    @Override
    public List<EBayItem> getAllItemsBySpecifiedAddress(String address) {
        return eBayItemDAO.findAllItemsBySpecifiedAddress(address);
    }
}
