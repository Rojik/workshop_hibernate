package cz.maros.service;

import cz.maros.dao.ContactDAO;
import cz.maros.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactDAO contactDAO;

    @Transactional
    @Override
    public List<Contact> getAllContactAcceptCC() {
        return contactDAO.findAllContactAcceptCC();
    }
}
