package cz.maros.dao;

import cz.maros.model.Contact;

import java.util.List;

public interface ContactDAO {

    public List<Contact> findAllContactAcceptCC();
}
