package cz.maros.dao;


import cz.maros.model.Category;

import java.util.List;

public interface CategoryDAO {

    public List<Category> findAllCategoriesWithObjectInCity(String address);

    public List<Category> findAllCategoriesNonLeaf();
}
