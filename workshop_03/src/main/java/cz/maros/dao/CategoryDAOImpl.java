package cz.maros.dao;

import cz.maros.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CategoryDAOImpl implements CategoryDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Category> findAllCategoriesWithObjectInCity(String address) {
        return entityManager.createQuery("SELECT c FROM Category c JOIN c.eBayItems i JOIN i.seller s JOIN s.addresses a WHERE a.city = :address")
                .setParameter("address", address).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Category> findAllCategoriesNonLeaf() {
        return entityManager.createQuery("SELECT DISTINCT c FROM Category c JOIN c.children child JOIN c.eBayItems i").getResultList();
    }
}
