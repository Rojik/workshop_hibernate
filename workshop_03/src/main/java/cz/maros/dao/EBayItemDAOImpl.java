package cz.maros.dao;

import cz.maros.model.EBayItem;
import cz.maros.model.PaymentType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EBayItemDAOImpl implements EBayItemDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<EBayItem> findAllItemsOrderPrice() {
        return entityManager.createQuery("SELECT i FROM EBayItem i order by i.pricePaid DESC").getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EBayItem> findAllItemsOrderByHigherPrice(double price) {
        return entityManager.createQuery("SELECT i FROM EBayItem i WHERE i.pricePaid > :price")
                .setParameter("price", price).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EBayItem> findAllItemsByLowerPriceAndPP(double price) {
        return entityManager.createQuery("SELECT i FROM EBayItem i JOIN i.seller s JOIN s.paymentMethods m WHERE i.priceAsked < :price AND m.type = :paymentType")
                .setParameter("paymentType", PaymentType.PP)
                .setParameter("price", price).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EBayItem> findAllItemsBySpecifiedAddress(String address) {
        return entityManager.createQuery("SELECT i FROM EBayItem i JOIN i.seller s JOIN s.paymentMethods m JOIN s.addresses a WHERE m.type = :paymentType AND a.city = :address")
                .setParameter("paymentType", PaymentType.CA)
                .setParameter("address", address).getResultList();
    }
}
