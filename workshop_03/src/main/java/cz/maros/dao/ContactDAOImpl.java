package cz.maros.dao;

import cz.maros.model.Contact;
import cz.maros.model.PaymentType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ContactDAOImpl implements ContactDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Contact> findAllContactAcceptCC() {
        return entityManager.createQuery("SELECT c FROM Contact c JOIN c.paymentMethods m WHERE m.type = :paymentType")
                .setParameter("paymentType", PaymentType.CC).getResultList();
    }
}
