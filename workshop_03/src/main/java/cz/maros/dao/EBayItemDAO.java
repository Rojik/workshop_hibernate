package cz.maros.dao;

import cz.maros.model.EBayItem;

import java.util.List;

public interface EBayItemDAO {

    public List<EBayItem> findAllItemsOrderPrice();

    public List<EBayItem> findAllItemsOrderByHigherPrice(double price);

    public List<EBayItem> findAllItemsByLowerPriceAndPP(double price);

    public List<EBayItem> findAllItemsBySpecifiedAddress(String address);
}
