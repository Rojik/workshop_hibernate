<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop</title>
</head>
<body>
<h2>Queries</h2>
<div><strong>1. getAllItemsOrderPrice</strong> <br />
    <p:forEach var="product" items="${allItemsOrderPrice}">
        <span>${product}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getAllItemsOrderByHigherPrice (30$)</strong> <br />
    <p:forEach var="product" items="${allItemsOrderByHigherPrice1}">
        <span>${product}</span> <br />
    </p:forEach> <br />

    <strong>2. getAllItemsOrderByHigherPrice (150$)</strong> <br />
    <p:forEach var="product" items="${allItemsOrderByHigherPrice2}">
        <span>${product}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>3. getAllContactAcceptCC</strong> <br />
    <p:forEach var="contact" items="${allContactAcceptCC}">
        <span>${contact}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>4. getAllItemsByLowerPriceAndPP (150$)</strong> <br />
    <p:forEach var="product" items="${allItemsByLowerPriceAndPP1}">
        <span>${product}</span> <br />
    </p:forEach> <br />

    <strong>4. getAllItemsByLowerPriceAndPP (50$)</strong> <br />
    <p:forEach var="product" items="${allItemsByLowerPriceAndPP2}">
        <span>${product}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>5. getAllItemsBySpecifiedAddress</strong> <br />
    <p:forEach var="product" items="${allItemsBySpecifiedAddress}">
        <span>${product}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>6. getAllCategoriesWithObjectInCity</strong> <br />
    <p:forEach var="category" items="${allCategoriesWithObjectInCity}">
        <span>${category}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>7. getAllCategoriesNonLeaf</strong> <br />
    <p:forEach var="category" items="${allCategoriesNonLeaf}">
        <span>${category}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h2>${completeQueries}</h2>
</body>
</html>