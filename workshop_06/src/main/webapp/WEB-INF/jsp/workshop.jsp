<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop</title>
</head>
<body>
<h2>Queries</h2>
<div><strong>1. getDungeonsByBoss</strong> <br />
    <p:forEach var="dungeon" items="${dungeonsByBoss}">
        <span>${dungeon}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getBossesByDungeon</strong> <br />
    <p:forEach var="boss" items="${bossesByDungeon}">
        <span>${boss}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getPlayersInDungeonWithBossStuff</strong> <br />
    <p:forEach var="player" items="${playersInDungeonWithBossStuff}">
        <span>${player}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h2>${completeQueries}</h2>
</body>
</html>