package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name = "W6_PLAYER")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PLAYER_ID_SEQ")
    @SequenceGenerator(name = "PLAYER_ID_SEQ", sequenceName = "PLAYER_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    @Override
    public String toString() {
        return "Player - ID: " + getId() + ", name: " + getName();
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
