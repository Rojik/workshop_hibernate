package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name = "W6_BOSS")
public class Boss {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BOSS_ID_SEQ")
    @SequenceGenerator(name = "BOSS_ID_SEQ", sequenceName = "BOSS_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @Basic
    @Column(name = "IS_EPIC_STAFF")
    private int isEpicStaff;

    @ManyToOne
    @JoinColumn(name = "DUNGEON_ID")
    private Dungeon dungeon;

    @Override
    public String toString() {
        return "Boss - ID: " + getId() + ", name: " + getName() + ", isEpicStaff: " + isEpicStaff;
    }

    public Dungeon getDungeon() {
        return dungeon;
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }

    public int getIsEpicStaff() {
        return isEpicStaff;
    }

    public void setIsEpicStaff(int isEpicStaff) {
        this.isEpicStaff = isEpicStaff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
