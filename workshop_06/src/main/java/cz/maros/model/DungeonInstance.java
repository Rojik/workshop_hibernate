package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name = "W6_DUNGEON_INSTANCE")
public class DungeonInstance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DUNGEON_INSTANCE_ID_SEQ")
    @SequenceGenerator(name = "DUNGEON_INSTANCE_ID_SEQ", sequenceName = "DUNGEON_INSTANCE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @OneToOne(mappedBy = "dungeonInstance")
    private Group group;

    @ManyToOne
    @JoinColumn(name = "DUNGEON_ID")
    private Dungeon dungeon;

    public Dungeon getDungeon() {
        return dungeon;
    }

    public void setDungeon(Dungeon dungeon) {
        this.dungeon = dungeon;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Long getId() {
        return id;
    }

}
