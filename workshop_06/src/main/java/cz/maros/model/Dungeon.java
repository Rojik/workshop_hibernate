package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W6_DUNGEON")
public class Dungeon {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DUNGEON_ID_SEQ")
    @SequenceGenerator(name = "DUNGEON_ID_SEQ", sequenceName = "DUNGEON_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "dungeon")
    private List<DungeonInstance> dungeonInstances = new ArrayList<DungeonInstance>();

    @OneToMany(mappedBy = "dungeon")
    private List<Boss> bosses = new ArrayList<Boss>();

    @Override
    public String toString() {
        return "Dungeon - ID: " + getId() + ", name: " + getName();
    }

    public List<Boss> getBosses() {
        return bosses;
    }

    public void setBosses(List<Boss> bosses) {
        this.bosses = bosses;
    }

    public List<DungeonInstance> getDungeonInstances() {
        return dungeonInstances;
    }

    public void setDungeonInstances(List<DungeonInstance> dungeonInstances) {
        this.dungeonInstances = dungeonInstances;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
