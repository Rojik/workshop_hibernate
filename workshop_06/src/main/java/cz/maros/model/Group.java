package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W6_GROUP")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "GROUP_ID_SEQ")
    @SequenceGenerator(name = "GROUP_ID_SEQ", sequenceName = "GROUP_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @OneToMany(mappedBy = "group")
    private List<Player> players = new ArrayList<Player>();

    @OneToOne
    @JoinColumn(name = "DUNGEON_INSTANCE_ID")
    private DungeonInstance dungeonInstance;

    public DungeonInstance getDungeonInstance() {
        return dungeonInstance;
    }

    public void setDungeonInstance(DungeonInstance dungeonInstance) {
        this.dungeonInstance = dungeonInstance;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Long getId() {
        return id;
    }

}
