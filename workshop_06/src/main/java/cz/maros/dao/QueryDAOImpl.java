package cz.maros.dao;

import cz.maros.model.Boss;
import cz.maros.model.Dungeon;
import cz.maros.model.Player;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class QueryDAOImpl implements QueryDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Dungeon> findDungeonsByBoss(String boss) {
        return entityManager.createQuery("SELECT d FROM Dungeon d JOIN d.bosses b WHERE b.name = :name")
                .setParameter("name", boss).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Boss> findBossesByDungeon(String dungeon) {
        return entityManager.createQuery("SELECT b FROM Boss b JOIN b.dungeon d WHERE d.name = :name")
                .setParameter("name", dungeon).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Player> findPlayersInDungeonWithBossStuff() {
        return entityManager.createQuery("SELECT DISTINCT p FROM Player p JOIN p.group g JOIN g.dungeonInstance di JOIN di.dungeon d JOIN d.bosses b WHERE b.isEpicStaff = :isEpicStaff")
                .setParameter("isEpicStaff", 1).getResultList();
    }
}
