package cz.maros.dao;

import cz.maros.model.Boss;
import cz.maros.model.Dungeon;
import cz.maros.model.Player;

import java.util.List;

public interface QueryDAO {

    public List<Dungeon> findDungeonsByBoss(String boss);

    public List<Boss> findBossesByDungeon(String dungeon);

    public List<Player> findPlayersInDungeonWithBossStuff();
}
