package cz.maros.service;

import cz.maros.dao.QueryDAO;
import cz.maros.model.Boss;
import cz.maros.model.Dungeon;
import cz.maros.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QueryServiceImpl implements QueryService {

    @Autowired
    private QueryDAO queryDAO;

    @Transactional
    @Override
    public List<Dungeon> getDungeonsByBoss(String boss) {
        return queryDAO.findDungeonsByBoss(boss);
    }

    @Transactional
    @Override
    public List<Boss> getBossesByDungeon(String dungeon) {
        return queryDAO.findBossesByDungeon(dungeon);
    }

    @Transactional
    @Override
    public List<Player> getPlayersInDungeonWithBossStuff() {
        return queryDAO.findPlayersInDungeonWithBossStuff();
    }
}
