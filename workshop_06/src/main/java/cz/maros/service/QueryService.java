package cz.maros.service;

import cz.maros.model.Boss;
import cz.maros.model.Dungeon;
import cz.maros.model.Player;

import java.util.List;

public interface QueryService {

    public List<Dungeon> getDungeonsByBoss(String boss);

    public List<Boss> getBossesByDungeon(String dungeon);

    public List<Player> getPlayersInDungeonWithBossStuff();


}
