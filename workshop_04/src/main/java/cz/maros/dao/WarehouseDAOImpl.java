package cz.maros.dao;

import cz.maros.model.ItemStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class WarehouseDAOImpl implements WarehouseDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Object findWarehouseWithIdleItem() {
//        return entityManager.createQuery("SELECT i.id, w FROM ItemStatus s INNER JOIN (SELECT MAX(s.dateStatus) FROM ItemStatus s INNER JOIN s.item i WHERE i.id = :item GROUP BY i.id) aa INNER JOIN s.item i INNER JOIN i.itemLocations loc INNER JOIN loc.warehouse w")
//                .setParameter("item", 4L).getResultList();
        return null;
    }
}

//return entityManager.createQuery("SELECT MAX(s.dateStatus) FROM ItemStatus s INNER JOIN s.item i WHERE i.id = :item GROUP BY i.id")
//        .setParameter("item", 4L).getSingleResult();
