package cz.maros.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TaskDAOImpl implements TaskDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public Object findMostUsedItemsInTask() {
        return entityManager.createQuery("SELECT t.name, COUNT(t.name) as _count FROM Task t JOIN t.items i GROUP BY t.name order by _count DESC").setMaxResults(1).getSingleResult();
    }

    @Override
    public String findUserWhichMostUsedEquipment(Long id) {
        return entityManager.createQuery("SELECT MAX(e.firstName) as max_employee FROM Employee e JOIN e.tasks t JOIN t.items i JOIN i.equipment equip WHERE equip.id = :equip_id GROUP BY e.firstName ORDER BY max_employee DESC")
                .setParameter("equip_id", id).setMaxResults(1).getSingleResult().toString();
    }
}
