package cz.maros.dao;

import cz.maros.model.Office;

public interface OfficeDAO {

    public Object findMostAttachedOfficeToWarehouse();
}
