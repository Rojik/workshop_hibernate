package cz.maros.dao;

import cz.maros.model.ItemStatus;

public interface WarehouseDAO {

    public Object findWarehouseWithIdleItem();
}
