package cz.maros.dao;

import java.util.List;

public interface TaskDAO {

    public Object findMostUsedItemsInTask();

    public String findUserWhichMostUsedEquipment(Long id);
}
