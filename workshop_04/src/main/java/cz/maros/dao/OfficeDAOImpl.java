package cz.maros.dao;

import cz.maros.model.Office;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class OfficeDAOImpl implements OfficeDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public Object findMostAttachedOfficeToWarehouse() {
        return entityManager.createQuery("SELECT o.name, COUNT(o.name) as _count FROM Office o JOIN o.warehouses w GROUP BY o.name ORDER BY _count DESC").setMaxResults(1).getSingleResult();
    }
}
