package cz.maros.service;

import cz.maros.dao.TaskDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Transactional
    @Override
    public Object getMostUsedItemsInTask() {
        return taskDAO.findMostUsedItemsInTask();
    }

    @Transactional
    @Override
    public String getUserWhichMostUsedEquipment() {
        return taskDAO.findUserWhichMostUsedEquipment(1L);
    }
}
