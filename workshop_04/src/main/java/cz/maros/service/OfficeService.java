package cz.maros.service;

import cz.maros.model.Office;

public interface OfficeService {

    public Object getMostAttachedOfficeToWarehouse();

}
