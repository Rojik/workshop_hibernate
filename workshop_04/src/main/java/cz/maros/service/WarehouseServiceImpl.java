package cz.maros.service;

import cz.maros.dao.WarehouseDAO;
import cz.maros.model.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseDAO warehouseDAO;

    @Override
    public Object getWarehouseWithIdleItem() {
        return warehouseDAO.findWarehouseWithIdleItem();
    }
}
