package cz.maros.service;

import cz.maros.dao.OfficeDAO;
import cz.maros.model.Office;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfficeServiceImpl implements OfficeService {

    @Autowired
    private OfficeDAO officeDAO;


    @Override
    public Object getMostAttachedOfficeToWarehouse() {
        return officeDAO.findMostAttachedOfficeToWarehouse();
    }
}
