package cz.maros.service;

import java.util.List;

public interface TaskService {

    public Object getMostUsedItemsInTask();

    public String getUserWhichMostUsedEquipment();
}
