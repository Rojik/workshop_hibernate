package cz.maros.service;

import cz.maros.model.ItemStatus;

public interface WarehouseService {

    public Object getWarehouseWithIdleItem();
}
