package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W4_STATUS_CODE")
public class StatusCode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "STATUS_CODE_ID_SEQ")
    @SequenceGenerator(name = "STATUS_CODE_ID_SEQ", sequenceName = "STATUS_CODE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy = "statusCode")
    private List<ItemStatus> itemStatuses = new ArrayList<ItemStatus>();

    public List<ItemStatus> getItemStatuses() {
        return itemStatuses;
    }

    public void setItemStatuses(List<ItemStatus> itemStatuses) {
        this.itemStatuses = itemStatuses;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
