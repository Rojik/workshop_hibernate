package cz.maros.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "W4_ITEM_STATUS")
public class ItemStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ITEM_STATUS_ID_SEQ")
    @SequenceGenerator(name = "ITEM_STATUS_ID_SEQ", sequenceName = "ITEM_STATUS_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_STATUS")
    @Temporal(TemporalType.DATE)
    private Date dateStatus;

    @Basic
    @Column(name = "DETAILS")
    private String details;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    @ManyToOne
    @JoinColumn(name = "STATUS_ID")
    private StatusCode statusCode;

    public StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Date getDateStatus() {
        return dateStatus;
    }

    public void setDateStatus(Date dateStatus) {
        this.dateStatus = dateStatus;
    }

    public Long getId() {
        return id;
    }
}
