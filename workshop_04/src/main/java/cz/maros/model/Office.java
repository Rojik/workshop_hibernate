package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W4_OFFICE")
public class Office {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "OFFICE_ID_SEQ")
    @SequenceGenerator(name = "OFFICE_ID_SEQ", sequenceName = "OFFICE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @Basic
    @Column(name = "ADDRESS")
    private String address;

    @Basic
    @Column(name = "DETAILS")
    private String details;

    public List<Warehouse> getWarehouses() {
        return warehouses;
    }

    public void setWarehouses(List<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @OneToMany(mappedBy = "office")
    List<Warehouse> warehouses = new ArrayList<Warehouse>();
}
