package cz.maros.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "W4_WAREHOUSE")
public class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "WAREHOUSE_ID_SEQ")
    @SequenceGenerator(name = "WAREHOUSE_ID_SEQ", sequenceName = "WAREHOUSE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @Basic
    @Column(name = "ADDRESS")
    private String address;

    @Basic
    @Column(name = "DETAILS")
    private String details;

    @OneToMany(mappedBy = "warehouse")
    private Set<ItemLocation> itemLocations = new HashSet<ItemLocation>();

    @ManyToOne
    @JoinColumn(name = "OFFICE_ID")
    private Office office;

    public Set<ItemLocation> getItemLocations() {
        return itemLocations;
    }

    public void setItemLocations(Set<ItemLocation> itemLocations) {
        this.itemLocations = itemLocations;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
