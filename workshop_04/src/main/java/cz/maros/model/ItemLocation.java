package cz.maros.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "W4_ITEM_LOCATION")
public class ItemLocation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "ITEM_LOCATION_ID_SEQ")
    @SequenceGenerator(name = "ITEM_LOCATION_ID_SEQ", sequenceName = "ITEM_LOCATION_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    @ManyToOne
    @JoinColumn(name = "WAREHOUSE_ID")
    private Warehouse warehouse;

    @Column(name = "DATE_FROM")
    @Temporal(TemporalType.DATE)
    private Date dateFrom;
}
