package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W4_EQUIPMENT")
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "EQUIPMENT_ID_SEQ")
    @SequenceGenerator(name = "EQUIPMENT_ID_SEQ", sequenceName = "EQUIPMENT_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "TYPE")
    private String type;

    @Basic
    @Column(name = "MODEL")
    private String model;

    @Basic
    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(mappedBy = "equipment")
    private List<Item> items = new ArrayList<Item>();

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
