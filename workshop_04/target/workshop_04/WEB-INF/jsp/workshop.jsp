<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop</title>
</head>
<body>
<h2>Queries</h2>
<p>---------------------------------------------------------------------------------</p>
<div><strong>1. getMostAttachedOfficeToWarehouse</strong> <br />
    <span>${mostAttachedOfficeToWarehouse[0]}</span> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getMostUsedItemsInTask</strong> <br />
    <span>${mostUsedItemsInTask[0]}</span> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>4. getUserWhichMostUsedEquipment</strong> <br />
    <span>${userWhichMostUsedEquipment}</span> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<h2>${completeQueries}</h2>
</body>
</html>