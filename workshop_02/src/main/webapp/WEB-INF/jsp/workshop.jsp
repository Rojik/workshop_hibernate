<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="div" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="ISO-8859-1"?>

<html>
<head>
    <title>Database workshop 02</title>
</head>
<body>
<h2>Queries</h2>
<div><strong>1. getAllSessionWithOneRegistration</strong> <br />
    <p:forEach var="session" items="${allSessionWithOneRegistration}">
        <span>${session}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>2. getCountSessionInFerbuary</strong> <br />
    <span>${allSessionInFerbuary}</span> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>3. getAllInstructorWithCourse - 1. course</strong> <br />
    <p:forEach var="instructor" items="${allInstructorWithCourse1}">
        <span>${instructor}</span> <br />
    </p:forEach> <br />

<strong>3. getAllInstructorWithCourse - 2. course</strong> <br />
    <p:forEach var="instructor" items="${allInstructorWithCourse2}">
        <span>${instructor}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>
<div><strong>4. getAllInstructorWithCourseAndDate - 1. course 1. date</strong> <br />
    <p:forEach var="instructor" items="${allInstructorWithCourseAndDate1}">
        <span>${instructor}</span> <br />
    </p:forEach> <br />

    <strong>4. getAllInstructorWithCourseAndDate - 2. course 2. date</strong> <br />
    <p:forEach var="instructor" items="${allInstructorWithCourseAndDate2}">
        <span>${instructor}</span> <br />
    </p:forEach> <br />
</div>
<p>---------------------------------------------------------------------------------</p>

<h2>${completeQueries}</h2>
</body>
</html>