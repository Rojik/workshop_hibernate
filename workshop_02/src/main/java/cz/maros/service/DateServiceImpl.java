package cz.maros.service;

import cz.maros.dao.DateDAO;
import cz.maros.model.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("dateService")
public class DateServiceImpl implements DateService {

    @Autowired
    private DateDAO dateDAO;

    @Transactional
    @Override
    public Date getDateById(Long id) {
        return dateDAO.findDateById(id);
    }
}
