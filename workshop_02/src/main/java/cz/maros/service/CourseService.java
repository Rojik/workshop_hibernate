package cz.maros.service;

import cz.maros.model.Course;

public interface CourseService {

    public Course getCourseById(Long id);
}
