package cz.maros.service;

import cz.maros.dao.SessionDAO;
import cz.maros.model.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private SessionDAO sessionDAO;

    @Transactional
    @Override
    public List<Session> getAllSessionWithOneRegistration() {
        return sessionDAO.findAllSessionWithOneRegistration();
    }


    @Transactional
    @Override
    public Long getCountSessionInFerbuary() {
        return sessionDAO.findAllSessionInFerbuary();
    }
}
