package cz.maros.service;


import cz.maros.dao.CourseDAO;
import cz.maros.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDAO courseDAO;

    @Transactional
    @Override
    public Course getCourseById(Long id) {
        return courseDAO.findCourseById(id);
    }
}
