package cz.maros.service;

import cz.maros.model.Course;
import cz.maros.model.Date;
import cz.maros.model.Instructor;

import java.util.List;

public interface InstructorService {

    public List<Instructor> getAllInstructorWithCourse(Course course);

    public List<Instructor> getAllInstructorWithCourseAndDate(Course course, Date date);
}
