package cz.maros.service;

import cz.maros.dao.InstructorDAO;
import cz.maros.model.Course;
import cz.maros.model.Date;
import cz.maros.model.Instructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class InstructorServiceImpl implements InstructorService {

    @Autowired
    private InstructorDAO instructorDAO;

    @Transactional
    @Override
    public List<Instructor> getAllInstructorWithCourse(Course course) {
        return instructorDAO.findAllInstructorWithCourse(course);
    }

    @Transactional
    @Override
    public List<Instructor> getAllInstructorWithCourseAndDate(Course course, Date date) {
        return instructorDAO.findAllInstructorWithCourseAndDate(course, date);
    }

}
