package cz.maros.service;

import cz.maros.model.Session;

import java.util.List;

public interface SessionService {

    public List<Session> getAllSessionWithOneRegistration();

    public Long getCountSessionInFerbuary();
}
