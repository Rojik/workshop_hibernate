package cz.maros.service;


import cz.maros.model.Date;

public interface DateService {

    public Date getDateById(Long id);
}
