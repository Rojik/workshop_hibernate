package cz.maros.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "W2_DATE")
public class Date {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "DATE_ID_SEQ")
    @SequenceGenerator(name = "DATE_ID_SEQ", sequenceName = "DATE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_START")
    @Temporal(TemporalType.DATE)
    private java.util.Date dateStart;

    @ManyToOne
    @JoinColumn(name = "SESSION_ID")
    private Session session;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public java.util.Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(java.util.Date dateStart) {
        this.dateStart = dateStart;
    }

    public Long getId() {
        return id;
    }
}
