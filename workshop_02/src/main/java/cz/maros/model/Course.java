package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W2_COURSE")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "COURSE_ID_SEQ")
    @SequenceGenerator(name = "COURSE_ID_SEQ", sequenceName = "COURSE_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "course")
    private List<Session> sessions = new ArrayList<Session>();

    @Override
    public String toString() {
        return "Course - ID: " + getId() + ", name: " + getName();
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
