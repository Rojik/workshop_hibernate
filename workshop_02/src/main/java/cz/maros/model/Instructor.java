package cz.maros.model;

import org.hibernate.annotations.Tables;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "W2_INSTRUCTOR")
public class Instructor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "INSTRUCTOR_ID_SEQ")
    @SequenceGenerator(name = "INSTRUCTOR_ID_SEQ", sequenceName = "INSTRUCTOR_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "NAME")
    private String name;

    @ManyToMany
    @JoinTable(
            name = "W2_INSTRUCTOR_SESSION",
            joinColumns = {@JoinColumn(name = "INSTRUCTOR_ID")},
            inverseJoinColumns = {@JoinColumn(name = "SESSION_ID")})
    private List<Session> sessions = new ArrayList<Session>();

    @Override
    public String toString() {
        return "Instructor - ID: " + getId() + ", name: " + getName();
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

}
