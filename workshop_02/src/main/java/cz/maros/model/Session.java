package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "W2_SESSION")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SESSION_ID_SEQ")
    @SequenceGenerator(name = "SESSION_ID_SEQ", sequenceName = "SESSION_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name = "ROOM")
    private String room;

    @OneToMany(mappedBy = "session")
    private List<Registration> registrations = new ArrayList<Registration>();

    @OneToMany(mappedBy = "session")
    private List<cz.maros.model.Date> dates = new ArrayList<cz.maros.model.Date>();

    @ManyToMany(mappedBy = "sessions")
    private List<Instructor> instructors = new ArrayList<Instructor>();

    @ManyToOne
    @JoinColumn(name = "COURSE_ID")
    private Course course;

    @Override
    public String toString() {
        return "Session - ID: " + getId() + ", room: " + getRoom() + ", course: " + getCourse();
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Instructor> getInstructors() {
        return instructors;
    }

    public void setInstructors(List<Instructor> instructors) {
        this.instructors = instructors;
    }

    public List<cz.maros.model.Date> getDates() {
        return dates;
    }

    public void setDates(List<cz.maros.model.Date> dates) {
        this.dates = dates;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public Long getId() {
        return id;
    }
}
