package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="W2_STUDENT")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "STUDENT_ID_SEQ")
    @SequenceGenerator(name = "STUDENT_ID_SEQ", sequenceName = "STUDENT_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="STUDENT_NAME")
    private String name;

    @OneToMany(mappedBy = "student")
    private List<Registration> registrations = new ArrayList<Registration>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }
}
