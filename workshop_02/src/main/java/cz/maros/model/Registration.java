package cz.maros.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="W2_REGISTRATION")
public class Registration {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "REGISTRATION_ID_SEQ")
    @SequenceGenerator(name = "REGISTRATION_ID_SEQ", sequenceName = "REGISTRATION_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Column(name = "DATE_REGISTRATION")
    @Temporal(TemporalType.DATE)
    private Date dateRegistration;

    @OneToOne
    @JoinColumn(name="COMPANY_ID")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "STUDENT_ID")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "SESSION_ID")
    private Session session;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    public Long getId() {
        return id;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

}
