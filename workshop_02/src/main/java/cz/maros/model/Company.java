package cz.maros.model;

import javax.persistence.*;

@Entity
@Table(name="W2_COMPANY")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "COMPANY_ID_SEQ")
    @SequenceGenerator(name = "COMPANY_ID_SEQ", sequenceName = "COMPANY_ID_SEQ")
    @Column(name = "ID")
    private Long id;

    @Basic
    @Column(name="NAME")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
