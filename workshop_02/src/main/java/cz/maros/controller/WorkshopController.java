package cz.maros.controller;

import cz.maros.dao.DateDAO;
import cz.maros.service.CourseService;
import cz.maros.service.DateService;
import cz.maros.service.InstructorService;
import cz.maros.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class WorkshopController {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private InstructorService instructorService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private DateService dateService;

    private static Logger log = LoggerFactory.getLogger(WorkshopController.class);

    @RequestMapping(value = "/")
    public ModelAndView actionDefault(ModelMap map) {

        ModelAndView model = new ModelAndView("workshop");

        getQueries(model);
        model.addObject("completeQueries", "Queries was completed");

        return model;
    }

    private void getQueries(ModelAndView model) {
        model.addObject("allSessionWithOneRegistration", sessionService.getAllSessionWithOneRegistration());
        model.addObject("allSessionInFerbuary", sessionService.getCountSessionInFerbuary());
        model.addObject("allInstructorWithCourse1", instructorService.getAllInstructorWithCourse(courseService.getCourseById(1L)));
        model.addObject("allInstructorWithCourse2", instructorService.getAllInstructorWithCourse(courseService.getCourseById(2L)));
        model.addObject("allInstructorWithCourse2", instructorService.getAllInstructorWithCourse(courseService.getCourseById(2L)));
        model.addObject("allInstructorWithCourseAndDate1", instructorService.getAllInstructorWithCourseAndDate(courseService.getCourseById(1L), dateService.getDateById(1L)));
        model.addObject("allInstructorWithCourseAndDate2", instructorService.getAllInstructorWithCourseAndDate(courseService.getCourseById(2L), dateService.getDateById(2L)));
    }
}
