package cz.maros.dao;

import cz.maros.model.Session;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class SessionDAOImpl implements SessionDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Session> findAllSessionWithOneRegistration() {
        return entityManager.createQuery("SELECT s FROM Session s JOIN s.registrations r").getResultList();
    }


    @SuppressWarnings("unchecked")
    @Override
    public Long findAllSessionInFerbuary() {
        return (Long) this.entityManager.createQuery("SELECT COUNT(s) FROM Date d JOIN d.session s WHERE MONTH(d.dateStart) = :month")
                .setParameter("month", 2).getSingleResult();
    }


}
