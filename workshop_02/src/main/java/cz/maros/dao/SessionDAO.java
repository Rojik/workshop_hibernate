package cz.maros.dao;

import cz.maros.model.Session;

import java.util.List;

public interface SessionDAO {

    public List<Session> findAllSessionWithOneRegistration();

    public Long findAllSessionInFerbuary();
}
