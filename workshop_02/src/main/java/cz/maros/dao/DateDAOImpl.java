package cz.maros.dao;

import cz.maros.model.Date;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository("dateDAO")
public class DateDAOImpl implements DateDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Date findDateById(Long id) {
        return entityManager.find(Date.class, id);
    }
}
