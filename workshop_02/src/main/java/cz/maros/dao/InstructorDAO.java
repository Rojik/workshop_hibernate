package cz.maros.dao;

import cz.maros.model.Course;
import cz.maros.model.Date;
import cz.maros.model.Instructor;

import java.util.List;

public interface InstructorDAO {

    public List<Instructor> findAllInstructorWithCourse(Course course);

    public List<Instructor> findAllInstructorWithCourseAndDate(Course course, Date date);
}
