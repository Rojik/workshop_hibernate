package cz.maros.dao;

import cz.maros.model.Course;
import cz.maros.model.Date;
import cz.maros.model.Instructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class InstructorDAOImpl implements InstructorDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<Instructor> findAllInstructorWithCourse(Course course) {
        return entityManager.createQuery("SELECT DISTINCT i FROM Instructor i JOIN i.sessions s JOIN s.course c WHERE c.id = :course_id")
                .setParameter("course_id", course.getId()).getResultList();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Instructor> findAllInstructorWithCourseAndDate(Course course, Date date) {
        return entityManager.createQuery("SELECT DISTINCT i FROM Instructor i JOIN i.sessions s JOIN s.course c JOIN s.dates d WHERE c.id = :course_id AND d.id = :date_id")
                .setParameter("course_id", course.getId())
                .setParameter("date_id", date.getId()).getResultList();
    }
}
