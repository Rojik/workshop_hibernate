package cz.maros.dao;

import cz.maros.model.Date;

public interface DateDAO {

    public Date findDateById(Long id);
}
