package cz.maros.dao;

import cz.maros.model.Course;

public interface CourseDAO {

    public Course findCourseById(Long id);
}
